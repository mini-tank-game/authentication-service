package main

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	var user, payload User
	defer r.Body.Close()

	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil || len(payload.Username) < 1 || len(payload.Password) < 1 {
		response := BuildResponse("fail", "Username dan password tidak boleh kosong")
		RespondJson(&w, http.StatusBadRequest, response)

		return
	}

	if status, _ := user.Login(payload.Username, payload.Password); !status {
		response := BuildResponse("fail", "Login failed, check again your credentials")

		RespondJson(&w, http.StatusNotFound, response)
	} else {
		response := BuildResponse("success", "Login success")

		sign := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"id":       user.ID,
			"username": user.Username,
		})

		token, _ := sign.SignedString(TOKEN_SECRETKEY)

		response["data"] = map[string]interface{}{
			"name":     user.Name,
			"email":    user.Email,
			"username": user.Username,
			"token":    "Bearer " + token,
		}

		RespondJson(&w, http.StatusOK, response)
	}
}

func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	var user User
	defer r.Body.Close()

	if err := json.NewDecoder(r.Body).Decode(&user); err != nil || len(user.Username) < 1 || len(user.Password) < 1 || len(user.Name) < 1 || len(user.Email) < 1 {
		response := BuildResponse("fail", "Incomplete registration data")
		RespondJson(&w, http.StatusBadRequest, response)

		return
	}

	if status, err := user.Register(user); !status {
		if strings.Contains(err.Error(), "unique") {
			var response map[string]interface{}

			if strings.Contains(err.Error(), "email") {
				response = BuildResponse("fail", "Email duplicate with other user")
			} else {
				response = BuildResponse("fail", "Username duplicate with other user")
			}

			RespondJson(&w, http.StatusBadRequest, response)
		} else {
			response := BuildResponse("error", "Internal Server Error")
			RespondJson(&w, http.StatusInternalServerError, response)
		}

	} else {
		response := BuildResponse("success", "Register success")
		// response["data"] = map[string]interface{}{
		// 	"name":     user.Name,
		// 	"email":    user.Email,
		// 	"username": user.Username,
		// }

		RespondJson(&w, http.StatusCreated, response)
	}
}
