package main

import (
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var DB *gorm.DB

func Setup() {
	var err error
	DB, err = gorm.Open("postgres", "host=postgresdb port=5432 sslmode=disable user=darmawan dbname=minitank_user_dev password=saputra")

	if err != nil {
		panic(err)
	} else {
		log.Println("Database connect!")
	}

	DB.AutoMigrate(&User{})
}
