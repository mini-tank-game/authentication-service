package main

import (
	"encoding/json"
	"net/http"
)

func BuildResponse(status, message string) map[string]interface{} {
	return map[string]interface{}{
		"status":  status,
		"message": message,
	}
}

func RespondJson(w *http.ResponseWriter, code int, payload map[string]interface{}) {
	(*w).Header().Set("Content-Type", "application/json")
	(*w).WriteHeader(code)

	json.NewEncoder(*w).Encode(payload)
}
