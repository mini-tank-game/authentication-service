package main

import (
	"fmt"

	"golang.org/x/crypto/bcrypt"

	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model

	Name     string `json:"name" gorm:"type:varchar(150)"`
	Username string `json:"username" gorm:"type:varchar(100);unique"`
	Password string `json:"password" gorm:"type:varchar(100)"`
	Email    string `json:"email" gorm:"type:varchar(150);unique"`
}

func (user *User) Login(username, password string) (bool, error) {
	err := DB.Where("username = ?", username).First(user).Error

	if err != nil {
		fmt.Println(err)
		return false, err
	}

	// Compare the password with hash
	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		fmt.Println(err)
		return false, err
	}

	return true, nil
}

func (user *User) Register(payload User) (bool, error) {
	// Hash the password
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(payload.Password), bcrypt.DefaultCost)
	payload.Password = string(hashedPassword)

	cb := DB.Create(&payload)

	if cb.Error != nil {
		fmt.Println(cb.Error.Error())
		return false, cb.Error
	}

	return true, nil
}
