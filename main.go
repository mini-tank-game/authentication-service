package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var TOKEN_SECRETKEY = []byte("minitank")

func main() {
	Setup()

	router := mux.NewRouter()

	defer DB.Close()

	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Auth Service v1")
	}).Methods("GET")
	router.HandleFunc("/auth/login", LoginHandler).Methods("POST")
	router.HandleFunc("/auth/register", RegisterHandler).Methods("POST")

	fmt.Println("Attempt to Listen on :4444")
	log.Fatal(http.ListenAndServe(":4444", router))

}
